
/**
 * 
 *
 * @module micro.js
 */

micro = {}

/**
 * Get document object by id attribute (similar to document.getElementById)
 * @param {string} a document object id
 * @returns {Object}
 */

micro.id = function(a)
{
	if (a) return document.getElementById(a); else return false;
}

/**
 * Global options
 */

micro.opts = 
{
	// default object (project)
	"default-project": "Arrow1",
	
	// project working files url
	"path": "http://sitis.ru/mpw-root",
	
	"php-path": "/php",
	
	// path to project files
	"folder": null
}

/**
 * Clear dashboard elements
 */

micro.emptyDashboard = function()
{
	var items
	
	items = [
	
		'input-object',
		'input-mosaic',
		'title',
		'thumbnails-info',
		'image-big-holder',
		'thumb-block',
		'canvas-holder'
	
	]
	
	for (var i in items)
	{
		var a = micro.id(items[i])
		if (a) a.innerHTML = ''
	}
	
	if (micro.id('cell-active')) micro.id('cell-active').parentNode.removeChild(micro.id('cell-active'))
}

/**
 * Load project
 * 
 * @param {String} el Project / object name
 */

micro.loadObject = function(el)
{
	micro.emptyDashboard()
	
	window.objects = {}
	window.env = {}
	window.opts.mosaic = 0
	micro.loader.start()
	var _el = el
	micro.scriptRequest(micro.opts.path + '/index.json?nocache=' + new Date().getTime(), function(data)
	{
		window.objects = {}
		
		if (typeof JSON == 'object')
		{
			window.objects = JSON.parse(data)
		}
		else
		{
			window.objects = json_parse(data)
		}
		
		if (!_el)
		{
			// show first project
			micro.opts.folder = window.objects[0]
		}
		else
		{
			micro.opts.folder = _el
		}
		
		var url = micro.opts.path + '/' + micro.opts.folder + '/index.json?nocache=' + new Date().getTime()
		
		yajax.send({
			"method": "GET",
			"url": url,
			"data": {"time": new Date().getTime()},
			"success": function(response)
			{
				micro.parseData(response)
			}
		})
		
	}, 'index-root')
	
	// below is deprecated
	// reveal project folder from GET query (?prj=)
	//var q = getQueryVariable('prj')
	//if (q) micro.opts.folder = q
}

/**
 * Load json file via iframe include
 * 
 * @param {String} url Path to json
 * @param {Object} unSucces Callback function
 * @param {String} process_id Iframe id
 * @deprecated since 0.5
 */

micro._scriptRequest = function(url, onSuccess, process_id)
{
	var ifr, el
	el = micro.id(process_id)
	
	// remove iframe
	if (el) el.parentNode.removeChild(el)
	if (url)
	{
		ifr = document.createElement('iframe')
		ifr.id = process_id
		ifr.width = '0px'
		ifr.height = '0px'
		ifr.border = '0'
		ifr.src = url
		
		//if (ifr.attachEvent) ifr.attachEvent('onload', onSuccess); else ifr.addEventListener('load', onSuccess, false);
		
		Event.add(ifr, 'load', onSuccess)
		document.body.appendChild(ifr)
	}
}

/**
 * Load .json file via AJAX
 * 
 * @param {String} url Path to json
 * @param {Object} unSucces Callback function
 * @param {String} process_id Iframe id
 */

micro.scriptRequest = function(url, onSuccess, process_id)
{
	yajax.send({
		"method": "GET",
		"url": url,
		"data": {"time": new Date().getTime()},
		"success": function(response)
		{
			if (typeof onSuccess == 'function') onSuccess(response)
		}
	})
}

/**
 * Prepare application data from JSON object (index.json from project dir)
 * @param {Object} data project .json data
 */

micro.parseData = function(data)
{
	if (typeof JSON == 'object')
	{
		window.load = JSON.parse(data)
	}
	else
	{
		window.load = json_parse(data)
	}
	
	for (var z in window.load)
	{
		switch (window.load[z]['type'])
		{
			case("objectinfo"):
				
				if (typeof window.env['objectinfo'] != 'object') window.env['objectinfo'] = {}
				window.env['objectinfo'] = window.load[z]
				
				break
				
			case("mosaic"):
				
				var files, rowCount = 0, colCount = 0, cell
				
				if (typeof window.env['mosaic'] != 'object') window.env['mosaic'] = []
				
				window.load[z].images = {}
				
				for (var i in window.load[z].pattern)
				{
					if (i == (rowCount * window.load[z].grid[0])) {rowCount++;colCount = 0;}
					cell = ((rowCount - 1) + '-' + (colCount))
					
					var l = 0
					
					window.load[z].images[cell] = []
					while (l < window.load[z].pattern[i])
					{
						var inc = parseInt(i)+1, inc1 = parseInt(l)+1
						window.load[z].images[cell][l] = micro.opts.path + '/' + micro.opts.folder + '/' + window.load[z].folder + '/0' + inc + '000' + inc1 + '.jpg'
						l++
					}
					
					colCount++
				}
				
				window.env.mosaic.push(window.load[z])
				
				break
		}
	}
	
	//console.debug(window.env)
	
	micro.init()
}

/**
 * Get iframe contents
 * @param {String} Iframe id
 * @deprecated since 0.5
 */

micro.getFrameContents = function(id)
{
	var iFrame = document.getElementById(id)
	var iFrameBody
	if (iFrame.contentDocument)
	{
		iFrameBody = iFrame.contentDocument.getElementsByTagName('body')[0]
	}
	else if (iFrame.contentWindow)
	{
		iFrameBody = iFrame.contentWindow.document.getElementsByTagName('body')[0]
	}
	return (iFrameBody)
}

/**
 * Loading progress
 */

micro.loader = 
{
	start: function()
	{
		if (!micro.id('wrap-opacity'))
		{
			// wrap opacity div around body
			var div = document.createElement("div")
			div.style.position = 'absolute'
			div.style.zIndex = '1000'
			div.style.left = '0'
			div.style.top = '0'
			div.style.width = document.body.offsetWidth + 'px'
			div.style.height = document.body.offsetHeight + 'px'
			div.style.background = "url(/img/tr.png)"
			div.id = "wrap-opacity"
			document.body.appendChild(div)
			
			// loading progress
			var div1 = document.createElement("div")
			div1.style.position = 'absolute'
			div1.style.zIndex = '2000'
			div1.style.width = '100px'
			div1.style.height = '15px'
			div1.style.textAlign = 'center'
			div1.style.left = (document.body.offsetWidth / 2) - 100 + 'px'
			div1.style.top = (document.body.offsetHeight / 2) - 15 + 'px'
			div1.style.padding = '30px'
			div1.style.border = '1px solid #ccc'
			div1.style.background = '#fff'
			div1.innerHTML = '<img src="/img/_loading.gif">'
			div1.className = 'shadow'
			div1.id = "wrap-progress"
			document.body.appendChild(div1)
		}
	},
	stop: function()
	{
		//micro.id('loading-progress').style.display = 'none'
		micro.id('wrap-opacity').parentNode.removeChild(micro.id('wrap-opacity'))
		micro.id('wrap-progress').parentNode.removeChild(micro.id('wrap-progress'))
	}
}

/**
 * Initialize. Collect image width and height.
 */

micro.init = function()
{
	if (typeof window.load == 'object')
	{
		var files, rowCount = 0, colCount = 0, cell
		
		micro.id('title').innerHTML = window.env['objectinfo'].text
		
		if (micro.id('image-big') !== null)
		{
			micro.id('image-big').parentNode.removeChild(micro.id('image-big'))
		}
		
		var el = new Image()
		el.id = 'image-big'
		el.style.visibility = 'hidden'
		el.className = 'shadow'
		var _el = el
		
		//micro.loader.start()
		
		el.onload = function()
		{
			window.env.imgWidth = _el.width
			window.env.imgHeight = _el.height
			
			micro.createOptions()
			
			micro.renderObjProps()
			
			micro.scaleBigImage()
			
			_el.style.visibility = 'visible'
			
			micro.id('photo-file').innerHTML = _el.src
			
			micro.fitToHeight()
			
			micro.renderGrid()
			
			micro.loader.stop()
		}
		
		el.src = micro.opts.path + '/' + micro.opts.folder + '/' + window.env['objectinfo'].image + '?nocache=' + new Date().getTime()
		
		// get block width before pushing image
		// height is calculating from browser viewport.height
		window.env.blockWidth = micro.id('block').offsetWidth
		micro.id('image-big-holder').appendChild(el)
	}
}

/**
 * Object properties
 * @param {String} mosId 
 */

micro.renderObjProps = function(mosId)
{
	var z = window.opts.mosaic
	
	if (window.env.mosaic[z])
	{
		micro.id('thumbnails-info').innerHTML = '<div style="font-size: 14px;border: 1px solid #ccc;padding: 7px">id: <b>' + window.env['objectinfo']['id'] + '</b><br>name: <b>' + window.env['objectinfo']['name'] + '</b><!--<br> objecttext: <b>' + window.env['objectinfo']['text'] + '</b> --><br>text: <b>' + window.env.mosaic[z].text + '</b> <br> zoom: <b>' + window.env.mosaic[z].zoom + '</b> <br> light: <b>' + window.env.mosaic[z].light + '</b><br> image: <b>' + window.env['objectinfo']['image'] + '</b></div>'
	}
}

/**
 * Scale left column big image
 */

micro.scaleBigImage = function()
{
	var width, height, origW, origH, ratio, maxWidth, maxHeight
	
	maxWidth  = window.env.blockWidth
	maxHeight = micro.viewport().height - micro.id('markup').offsetHeight - 65
	
	window.env.origWidth = width = micro.id('image-big').width
	window.env.origHeight = height = micro.id('image-big').height
	
	micro.scaleImage(maxWidth, maxHeight, 'image-big', true)
}

/**
 * Scale image
 * @flag - TRUE - put ratio into globals
 */

micro.scaleImage = function(maxWidth, maxHeight, img, flag)
{
	var width, height
	
	width = micro.id(img).width
	height = micro.id(img).height
	
	if (width > maxWidth)
	{
		if (flag) window.env.ratioW = ratio = maxWidth / width; else ratio = maxWidth / width;
		micro.id(img).style.width = maxWidth + 'px'
		micro.id(img).style.height = (height * ratio) + 'px'
		height = height * ratio
		width = width * ratio
	}
	
	if (height > maxHeight)
	{
		if (flag) window.env.ratioH = ratio = maxHeight / height; else ratio = maxHeight / height;
		micro.id(img).style.height = maxHeight + 'px'
		micro.id(img).style.width = (width * ratio) + 'px'
		width = width * ratio
	}
}

/**
 * Create <select> options for objects and mosaics
 */

micro.createOptions = function()
{
	var select, option
	
	// create mosaic options
	select = document.createElement('select')
	select.style.padding = '3px'
	select.onchange = function()
	{
		micro.loadMosaic(this.value)
	}
	
	for (var o in window.env.mosaic)
	{
		option = document.createElement("option")
		option.setAttribute("value", window.env.mosaic[o]['id'])
		option.innerHTML = window.env.mosaic[o]['name']
		select.appendChild(option)
	}
	
	select1 = document.createElement('select')
	select1.style.padding = '3px'
	select1.onchange = function()
	{
		micro.loadObject(this.value)
	}
	
	for (var o in window.objects)
	{
		option = document.createElement("option")
		if (micro.opts.folder == window.objects[o]) option.setAttribute("selected", "selected")
		option.setAttribute("value", window.objects[o])
		option.innerHTML = window.objects[o]
		select1.appendChild(option)
	}
	
	micro.id('input-mosaic').innerHTML = ''
	micro.id('input-mosaic').appendChild(select)
	micro.id('input-object').innerHTML = ''
	micro.id('input-object').appendChild(select1)
}

/**
 * Viewport size
 * @ return {Object} width and height in object attributes
 */

micro.viewport = function()
{
	var viewPortWidth
	var viewPortHeight
	
	if (typeof window.innerWidth != 'undefined')
	{
		viewPortWidth = window.innerWidth,
		viewPortHeight = window.innerHeight
	} else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
	{
		viewPortWidth = document.documentElement.clientWidth,
		viewPortHeight = document.documentElement.clientHeight
	} else {
		viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
		viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
	}
	return {"width": viewPortWidth, "height": viewPortHeight}
}

/**
 * Clear right block elements
 */

micro.clearThumbs = function()
{
	micro.id('photo-amount').innerHTML = 0
	micro.id('row').innerHTML = ''
	micro.id('col').innerHTML = ''
	if (micro.id('preview-image')) micro.id('preview-image').parentNode.removeChild(micro.id('preview-image'))
	micro.id('photo-file').parentNode.parentNode.style.visibility = 'hidden'
	micro.id('photo-width').parentNode.parentNode.style.visibility = 'hidden'
}

/**
 * Scale block to viewport height
 */

micro.fitToHeight = function()
{
	var need = micro.viewport().height - 50
	var h = micro.id('block').getBoundingClientRect().top + need
	
	if (window['isIE']) var add = 18; else var add = 20;
	
	micro.id('block').style.height = h + 'px'
	micro.id('block-right').style.height = h + add +'px'
}

/**
 * Render thumbnails
 * @param {String} row position X
 * @param {String} col position Y
 */

micro.renderThumbnails = function(row, col)
{
	var z = window.opts.mosaic
	
	if (typeof window.env.mosaic[z]['defaults'] == 'object')
	{
		if (typeof row != 'number' && typeof col != 'number')
		{
			var row, col, mosaic_id
			
			mosaic_id = z
			var row = window.env.mosaic[z]['defaults'][0]
			var col = window.env.mosaic[z]['defaults'][1]
		}
	}
	
	if (typeof row != 'number' && typeof col != 'number')
	{
		micro.clearThumbs()
		return
	}
	
	// old rendering method
	/*
	var count = 0
	
	for (var st in window.env.mosaic[z].images[row + '-' + col])
	{
		micro.id('thumb-block').innerHTML += '<div style="display: inline-block;text-align: center;float: left"><img src="' + window.env.mosaic[z].images[row + '-' + col][st] + '" width="80px" style="margin-right: 5px;background: #fff;cursor: pointer" onclick="micro.loadPreview(\'' + window.env.mosaic[z].images[row + '-' + col][st] + '\')"><br>0' + st + '</div>'
		count++
	}
	*/
	
	//alert(window.env.mosaic[z].pattern)
	
	var path = micro.opts.path + '/' + micro.opts.folder + '/' + window.env.mosaic[z].folder
	var rowstep = row * window.env.mosaic[z].grid[0]
	var endpoint = window.env.mosaic[z].pattern[rowstep + col]
	
	yajax.send({
		"method": "POST",
		"url": document.URL + micro.opts["php-path"] + "/scan.php",
		"data": {"src": escape(micro.opts.folder + '/' + window.env.mosaic[z].folder)},
		"success": function(response)
		{
			window.mosaics = json_parse(response)
			
			var summ = 0
			for (var j = 0;j < (rowstep + col);j++)
			{
				summ = summ + window.env.mosaic[z].pattern[j]
			}
			
			var filename, _filename, count = 0
			for (var k = 0;k < endpoint;k++)
			{
				var _filename = k + summ
				var filename  = window.mosaics[_filename]
				
				micro.id('thumb-block').innerHTML += '<div style="display: inline-block;text-align: center;float: left"><img src="' + document.URL + micro.opts["php-path"] + '/image-resize.php?path=' + micro.opts.folder + '/' + window.env.mosaic[z].folder + '/' + filename + '" width="80px" style="margin-right: 5px;background: #fff;cursor: pointer" onclick="micro.loadPreview(\'' + path + '/' + filename + '\')"><br>' + count + '</div>'
				count++
			}
			
			// no photos in the block
			if (count == 0)
			{
				micro.clearThumbs()
			}
			else
			{
				var _filename = summ
				var filename  = window.mosaics[_filename]
				micro.loadPreview(path + '/' + filename)
				micro.id('photo-file').parentNode.parentNode.style.visibility = 'visible'
				micro.id('photo-width').parentNode.parentNode.style.visibility = 'visible'
			}
			
			micro.id('photo-amount').innerHTML = count
			micro.id('row').innerHTML = row
			micro.id('col').innerHTML = col
		}
	})
	

}

/**
 * Change preview
 * @param {String} url Image url
 */

micro.loadPreview = function(url)
{
	if (micro.id('preview-image')) micro.id('preview-image').parentNode.removeChild(micro.id('preview-image'))
	
	if (!micro.id('preview-image'))
	{
		var img = new Image()
		img.style.visibility = 'hidden'
		img.style.fontSize = '1px'
		//img.setAttribute('class', 'autoH')
		//img.className = 'autoH'
		img.id = 'preview-image'
		var _img = img
		img.onload = function()
		{
			var maxWidth  = window.env.blockWidth - 30
			var maxHeight = micro.viewport().height - micro.id('markup').offsetHeight - 65
			//alert(maxHeight)
			if (!micro.id('preview-image'))
			{
				micro.id('preview').appendChild(img)
				img.style.visibility = 'visible'
				
				micro.id('photo-width').innerHTML = this.width
				micro.id('photo-height').innerHTML = this.height
				
				micro.scaleImage(maxWidth, maxHeight, 'preview-image', false)
				
				//micro.id('photo-width').innerHTML = micro.id('preview-image').style.width
				//micro.id('photo-height').innerHTML = micro.id('preview-image').style.height
				
				micro.id('photo-file').innerHTML = url
				//micro.id('photo-filesize').innerHTML = _img.fileSize
			}
		}
		img.src = url
	}
}

/**
 * html grid render
 * @param {String} _row Grid row
 * @param {String} _col Grid column
 * @param {String} _el Pass the dom element
 */

micro.renderGrid = function(_row, _col, _el)
{
	window.env.cells = {}
	
	// clear before using
	micro.id('thumb-block').innerHTML = ''
	
	var z = window.opts.mosaic, d, w, h, cellW, cellH, offsetW, offsetH, ctx, count = 0, out = '', add, posT, posL
	
	// check for default cell in mosaic
	if (typeof _row != 'number' && typeof _col != 'number')
	{
		//if (window.env.mosaic[z].default)
		//console.debug(window.env.mosaic[z])
		if (typeof window.env.mosaic[z].defaults == 'object')
		{
			_row = window.env.mosaic[z].defaults[0]
			_col = window.env.mosaic[z].defaults[1]
		}
	}
	
	cellW = window.env.mosaic[z].cell[0]
	cellH = window.env.mosaic[z].cell[1]
	
	offsetW = window.env.mosaic[z].offset[0]
	offsetH = window.env.mosaic[z].offset[1]
	
	if (!window.env.ratioW) window.env.ratioW = 1
	if (!window.env.ratioH) window.env.ratioH = 1
	
	// scale offset
	if (window.env.ratioW)
	{
		offsetW = window.env.mosaic[z].offset[0] * window.env.ratioW
		offsetH = window.env.mosaic[z].offset[1] * window.env.ratioW
	}
	
	if (window.env.ratioH)
	{
		offsetW = offsetW * window.env.ratioH
		offsetH = offsetH * window.env.ratioH
	}
	
	// scale cell
	if (window.env.ratioW)
	{
		w = window.env.ratioW * cellW
		h = window.env.ratioW * cellH
	}
	
	if (window.env.ratioH)
	{
		w = window.env.ratioH * w
		h = window.env.ratioH * h
	}
	
	// canvas div holder
	var el = micro.id('canvas-holder')
	
	el.innerHTML = ''
	el.style.position = 'absolute'
	el.style.left = micro.id('image-big').getBoundingClientRect().left + offsetW + 'px'
	el.style.top = micro.id('image-big').getBoundingClientRect().top + offsetH + 'px'
	el.style.width = '1000px'
	el.style.height = '1000px'
	micro.id('main-container').appendChild(el)
	
	var border
	
	if (window.env.mosaic[z].color)
	{
		border = window.env.mosaic[z].color[0] + ',' + window.env.mosaic[z].color[1] + ',' + window.env.mosaic[z].color[2]
	}
	else
	{
		border = '0, 0, 0'
	}
	
	// add property to css directly
	var style = document.createElement('style')
	style.type = 'text/css'
	var cssStr = '.border-color { border: 1px solid rgb(' + border + '); }'
	
	if (style.styleSheet)
	{
		style.styleSheet.cssText = cssStr;
	}
	else
	{
		var cssText = document.createTextNode(cssStr)
		style.appendChild(cssText)
	}
	
	document.getElementsByTagName('head')[0].appendChild(style)
	
	//console.debug(document.styleSheets)
	
	for (var i = 0;i < window.env.mosaic[z].grid[1];i++)
	{
		for (var c = 0;c < window.env.mosaic[z].grid[0];c++)
		{
			posL = (c * w) + 'px'
			posT = (i * h) + 'px'
			
			if (i > 0) add = 'border-top: 0;'; else add = '';
			
			if (c > 0) out += '<div class="cell border-color" id="' + i + '-' + c + '-' + z + '" onclick="micro.renderGrid(' + i + ', ' + c + ', this, \'' + z + '\')" style="width: ' + w + 'px;height: ' + h + 'px;' + add + 'border-left: 0;position: absolute;overflow: hidden;left: ' + posL + ';top: ' + posT + '"></div>'; else out += '<div class="cell border-color" onclick="micro.renderGrid(' + i + ', ' + c + ', this, \'' + z + '\')" id="' + i + '-' + c + '-' + z + '" style="width: ' + w + 'px;height: ' + h + 'px;' + add + 'overflow: hidden;position: absolute;overflow: hidden;display: block;left: ' + posL + ';top: ' + posT + '"></div>';
		}
	}
	
	micro.id('canvas-holder').innerHTML = out
	
	if (typeof _row == 'number' && typeof _col == 'number')
	{
		if (micro.id('cell-active')) micro.id('cell-active').parentNode.removeChild(micro.id('cell-active'))
		var activeX, activeY, activeW, activeH
		
		var obj = micro.id(_row + '-' + _col + '-' + z)
		
		activeX = obj.getBoundingClientRect().left
		activeY = obj.getBoundingClientRect().top
		
		activeW = obj.offsetWidth
		activeH = obj.offsetHeight
		
		var _offset
		
		if (window.env.ratioW)
		{
			_offset = window.env.mosaic[z].cell[2] * window.env.ratioH
		}
		
		if (window.env.ratioH)
		{
			_offset = _offset * window.env.ratioH
		}
		
		var div = document.createElement('div')
		div.style.position = 'absolute'
		div.id = 'cell-active'
		div.style.overflow = 'hidden'
		div.style.left = activeX - (_offset / 2) + 'px'
		div.style.top = activeY - (_offset / 2) + 'px'
		
		if (window['isIE']) var add = 0; else var add = 2
		
		div.style.width = activeW + _offset - add + 'px'
		div.style.height = activeH + _offset - add + 'px'
		div.style.border = '2px solid green'
		micro.id('main-container').appendChild(div)
	}
	
	micro.renderThumbnails(_row, _col)
	micro.renderObjProps()
}

/**
 * Load mosaic
 * @param {String} id Mosaic id
 */

micro.loadMosaic = function(id)
{
	var el = micro.id('cell-active')
	if (el) el.parentNode.removeChild(el)
	window.opts.mosaic = id
	micro.renderGrid(id)
}

/**
 * <canvas> grid render
 * @deprecated since 0.5 but may be used in the future
 */

micro._renderGrid = function(_row, _col)
{
	window.env.cells = {}
	
	var z = window.opts.mosaic
	var d, w, h, x, y, cellW, cellH, offsetW, offsetH, ctx, count = 0
	
	cellW = window.env.mosaic[z].cell[0]
	cellH = window.env.mosaic[z].cell[1]
	
	// canvas div holder
	var el = micro.id('canvas-holder')
	el.innerHTML = ''
	
	el.style.left = micro.id('image-big').getBoundingClientRect().left + 'px'
	el.style.top = micro.id('image-big').getBoundingClientRect().top + 'px'
	el.style.width = '1000px'
	el.style.height = '1000px'
	
	for (var cols = 0;cols < window.env.mosaic[z].grid[1];cols++)
	{
		for (var rows = 0;rows < window.env.mosaic[z].grid[0];rows++)
		{
			var canvas = micro.id('canvas-'+ cols + '-' + rows)
			if (!canvas)
			{
				window.env.cells[cols+'-'+rows] = document.createElement('canvas')
				window.env.cells[cols+'-'+rows].id = 'canvas-' + cols + '-' + rows
			}
			else
			{
				window.env.cells[cols+'-'+rows] = canvas
			}
			
			if (typeof G_vmlCanvasManager != 'undefined')
			{
				G_vmlCanvasManager.initElement(window.env.cells[cols+'-'+rows])
			}
			
			if (window.env.ratioW)
			{
				w = window.env.ratioW * cellW
				h = window.env.ratioW * cellH
			}
			
			if (window.env.ratioH)
			{
				w = window.env.ratioH * w
				h = window.env.ratioH * h
			}
			
			window.env.cells[cols+'-'+rows].style.width = w + 'px'
			window.env.cells[cols+'-'+rows].style.height = h + 'px'
			window.env.cells[cols+'-'+rows].style.position = 'absolute'
			window.env.cells[cols+'-'+rows].style.left = micro.id('image-big').getBoundingClientRect().left + (cols * w) + 'px'
			window.env.cells[cols+'-'+rows].style.top = micro.id('image-big').getBoundingClientRect().top + (rows * h) + 'px'
			
			micro.id('canvas-holder').appendChild(window.env.cells[cols+'-'+rows])
			ctx = window.env.cells[cols+'-'+rows].getContext('2d')
			
			x = w
			y = h
			
			if (rows == (window.env.mosaic[z].grid[0] - 1))
			{
				x = w
				y = h + 1
			}
			
			if (cols == (window.env.mosaic[z].grid[1] - 1))
			{
				x = w + 1
				y = h
			}
			
			window.env.cells[cols+'-'+rows].width = x
			window.env.cells[cols+'-'+rows].height = y
			
			window.env.cells[cols+'-'+rows].onclick = function()
			{
				alert('clicked !')
			}
			
			ctx.strokeStyle = "#000000"
			ctx.lineWidth = 1
			//ctx.globalAlpha = 0.2
			ctx.rect(0.5, 0.5, w, h)
			ctx.stroke()
			//ctx.fill()
			
			count++
		}
	}
}

/**
 * Start
 */

micro.start = function()
{
	window.opts = {}
	
	// default object
	window.opts.object = micro.opts["default-project"]
	window.opts.mosaic = 0
	
	micro.loadObject(window.opts.object)
}

