
/*
 * Asynchronous XMLHttprequest standalone micro-lib
 *
 * Usage:
 * yajax.send({
 * 		"method": "POST", // (GET, POST, PUT ... uppercase letters)
 *		"url": "/some/url.py",
 *		"data": {"src": "dd.jpg", "left": 533},
 *		"success": function(response) {alert(response);}
 * })
 *
 * @copyright Dmitry B
 */

yajax = {
	
	/*
	 * get xmlhttprequest instance
	 */
	
	http: function()
	{
		var f, i, a, b;
		
		f = [
			function() { return new XMLHttpRequest(); },
			function() { return new ActiveXObject("Msxml2.XMLHTTP"); },
			function() { return new ActiveXObject("Microsoft.XMLHTTP"); }
		];
		for (i = 0; i < f.length; i++)
		{
			try
			{
				a = f[i];
				b = a();
				if (b != null) return b;
			}
			catch (e)
			{
				continue;
			}
			throw new Error("XMLHttpRequest is not supported");
		}
	},
	
	/*
	 * data object to url string
	 */
	
	str: function(data)
	{
		var out, _out;
		
		_out = '';
		for (var a in data)
		{
			_out += a + "=" + data[a] + "&";
		}
		out = _out.substr(0, _out.length - 1);
		return out;
	},
	
	/*
	 * send data
	 */
	
	send: function(obj)
	{
		var a, callback, prfx, p, m, h;
		
		m = obj.method;
		h = this.http();
		p = this.str(obj.data);
		callback = obj.success;
		if (m == 'POST')
		{
			h.open(m.toUpperCase(), obj.url, true);
			h.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
			h.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			h.send(p);
		}
		else if (m == 'GET')
		{
			h.open(m.toUpperCase(), obj.url + "?" + p, true);
			h.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			h.send();
		}
		else
		{
			h.open(m.toUpperCase(), obj.url, true);
			h.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			h.send();
		}
		if (typeof obj.success == 'function')
		{
			function timer(callback)
			{
				setTimeout(function()
				{
					if (h.readyState == 4)
					{
						callback(h.responseText);
					}
					else
					{
						timer(callback);
					}
				});
			}
			timer(obj.success);
		}
	}
}

