<?php

/**
 * Scan folder for .jpg files
 *
 * @param string $input
 * @return string Returns JSON encoded string
 */

function scan($input)
{
	$dir = new DirectoryIterator($input);
	$collect = array();
	foreach ($dir as $fileinfo)
	{
		if (!$fileinfo->isDot())
		{
			$filename = $fileinfo->getFilename();
			$chunk    = explode('-', $filename);
			$size     = count($chunk);
			//$key = trim($chunk[$size - 1], '.jpg');
			$key = str_replace('.jpg', '', $chunk[$size - 1]);
			$collect[$key] = $filename;
		}
	}
	
	asort($collect);
	
	$i = 0;
	foreach ($collect as $k => $v)
	{
		$out[$i] = $v;
		$i++;
	}
	echo json_encode($out);
}

$prefix = urldecode($_POST['src']);
include('config.php');
scan($path . $prefix . '/');

?>